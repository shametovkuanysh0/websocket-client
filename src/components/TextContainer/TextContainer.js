import React from 'react';

import onlineIcon from '../../icons/onlineIcon.png';
import ColumnTic from './ColumnTic/ColumnTic';

import './TextContainer.css';

const TextContainer = ({ users }) => (


  <div className="textContainer">
    <div>
      <div className='tic-tac'>
        <div className='row'>
          <ColumnTic />
          <ColumnTic />
          <ColumnTic />
        </div>
        <div className='row'>
          <ColumnTic />
          <ColumnTic />
          <ColumnTic />
        </div>
        <div className='row'>
          <ColumnTic />
          <ColumnTic />
          <ColumnTic />
        </div>
      </div>
    </div>
    {
      users
        ? (
          <div>
            <h1>People currently chatting:</h1>
            <div className="activeContainer">
              <h2>
                {users.map(({name}) => (
                  <div key={name} className="activeItem">
                    {name}
                    <img alt="Online Icon" src={onlineIcon}/>
                  </div>
                ))}
              </h2>
            </div>
          </div>
        )
        : null
    }
  </div>
);

export default TextContainer;